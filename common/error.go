package common

import (
    "fmt"
)

type ValidationFailed struct {
    message string
}

func ValidationFailedErr(message string) *ValidationFailed {
    return &ValidationFailed{message}
}

func (e *ValidationFailed) Error() string {
    return fmt.Sprintf("Validation failed: %s", e.message)
}
