package common

import (
    "net/http"
)

const HTTP_GET = "GET"
const HTTP_POST = "POST"
const HTTP_PUT = "PUT"

type HttpHandler func(http.ResponseWriter, *http.Request)
type RouterFactory func(string) (http.Handler, error)
