package common

type Data interface{}
type Monad func(Data) (Data, error)

func Next(fn Monad, nextFn Monad) Monad {
    return func(data Data) (Data, error) {
        if newData, err := fn(data); err == nil {
            return nextFn(newData)
        } else {
            return nil, err
        }
    }
}

func Check(err error) Monad {
    return func(data Data) (Data, error) {
        if err == nil {
            return nil, nil
        } else {
            return nil, err
        }
    }
}

func MaybeExec(err error) func(Monad) (Data, error) {
    if err == nil {
        return Exec
    } else {
        return func(fn Monad) (Data, error) {
            return nil, err
        }
    }
}

func Exec(fn Monad) (Data, error) {
    return fn(nil)
}

func MaybeExecTwo(err error) func(Monad, Monad) (Data, Data, error) {
    if err == nil {
        return ExecTwo
    } else {
        return func(fn Monad, fn2 Monad) (Data, Data, error) {
            return nil, nil, err
        }
    }
}

func ExecTwo(fn Monad, fn2 Monad) (Data, Data, error) {
    if data, err := fn(nil); err == nil {
        if data2, err2 := fn2(nil); err2 == nil {
            return data, data2, nil
        } else {
            return nil, nil, err2
        }
    } else {
        return nil, nil, err
    }
}
