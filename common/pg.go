package common

import (
    "database/sql"
)

type PgConnection interface {
    Client() *sql.DB
}

func CreatePgConnection(connString string) (PgConnection, error) {
    if conn, err := sql.Open("postgres", connString); err == nil {
        inst := &pg{
            conn: conn,
        }
        return inst, nil
    } else {
        return nil, err
    }
}

type pg struct {
    conn *sql.DB
}

func (s *pg) Client() *sql.DB {
    return s.conn
}
