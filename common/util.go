package common

import (
    "io"
    "os"
    "encoding/json"
    "gopkg.in/go-playground/validator.v9"
    "github.com/gorilla/mux"
)

type Deserializer func(io.ReadCloser) (interface{}, error)

type JsonSerializable interface {
    ToJson() ([]byte, error)
}

type NoArgMonad func() (Data, error)

type ResponseHandler func(interface{})

func OpenFile(filename string) Monad {
    return func(data Data) (Data, error) {
        return os.Open(filename)
    }
}

func DecodeJson(body Data) Monad {
    return func(data Data) (Data, error) {
        file := data.(*os.File)
        decoder := json.NewDecoder(file)
        err := decoder.Decode(body)
        return body, err
    }
}

func Validate() Monad {
    return func(data Data) (Data, error) {
        return data, validator.New().Struct(data)
    }
}

func DeserializeWith(deserialize Deserializer) Monad {
    return func(data Data) (Data, error) {
        stream := data.(io.ReadCloser)
        return deserialize(stream)
    }
}

func SerializeToJson() Monad {
    return func(data Data) (Data, error) {
        obj := data.(JsonSerializable)
        return obj.ToJson()
    }
}

func With(fn Monad, arg Data) Monad {
    return func(data Data) (Data, error) {
        return fn(arg)
    }
}

func Respond(onSuccess ResponseHandler, onFailure ResponseHandler) func(Monad) {
    return func(fn Monad) {
        if data, err := fn(nil); err == nil {
            onSuccess(data)
        } else {
            onFailure(err)
        }
    }
}

func MountRouter(createRouter RouterFactory, prefix string) Monad {
    return func(data Data) (Data, error) {
        mux := data.(*mux.Router)
        if router, err := createRouter(prefix); err == nil {
            mux.PathPrefix(prefix).Handler(router)
            return mux, nil
        } else {
            return nil, err
        }
    }
}
