module backend

go 1.13

require (
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/gorilla/mux v1.7.4
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/lib/pq v1.5.2
	github.com/urfave/negroni v1.0.0
	gopkg.in/go-playground/validator.v9 v9.31.0
)
