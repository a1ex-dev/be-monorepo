package main

import (
    "backend/testnode"
    "log"
    "os"
)

func main() {
    confFilename := os.Args[1]
    if conf, err := testnode.ReadConfig(confFilename); err == nil {
        if app, err := testnode.CreateApp(conf); err == nil {
            app.Run()
        } else {
            log.Fatal(err)
        }
    } else {
        log.Fatal(err)
    }
}
