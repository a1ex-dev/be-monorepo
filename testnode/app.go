package testnode

import (
    "os"
    "os/signal"
    "log"
    "net/http"
    "github.com/gorilla/mux"
    "backend/testnode/cat"
    "backend/testnode/dog"
    c "backend/common"
    d "backend/testnode/domain"
)

func CreateApp(conf d.Config) (c.App, error) {
    if storage, err := CreateStorage(conf); err == nil {
        inst := &app{
            signals: make(chan os.Signal, 1),
            conf: conf,
            storage: storage,
        }
        signal.Notify(inst.signals, os.Interrupt)
        return inst, nil
    } else {
        return nil, err
    }
}

type app struct {
    signals chan os.Signal
    conf d.Config
    storage d.Storage
}

func (s *app) Config() d.Config {
    return s.conf
}

func (s *app) Storage() d.Storage {
    return s.storage
}

func (s *app) runServer(router http.Handler) {
    hostname := s.Config().Hostname()
    log.Printf("Server is starting on %s", hostname)
    log.Fatal(http.ListenAndServe(
        hostname, router.(*mux.Router)))
}

func (s *app) Run() {
    wire := c.MountRouter(cat.NewCatRouter(s), "/cat")
    wire = c.Next(wire, c.MountRouter(dog.NewDogRouter(s), "/dog"))
    if router, err := wire(mux.NewRouter()); err == nil {
        go s.runServer(router.(http.Handler))
        <-s.signals
        log.Printf("Shutting down")
    } else {
        panic(err)
    }
}
