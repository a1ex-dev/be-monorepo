package cat

import (
    "fmt"
)

type CatNotFound struct {
    catId int
}

func CatNotFoundErr(catId int) *CatNotFound {
    return &CatNotFound{catId}
}

func (e *CatNotFound) Error() string {
    return fmt.Sprintf("Cat not found: %d", e.catId)
}
