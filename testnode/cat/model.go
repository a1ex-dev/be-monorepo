package cat

import (
    "encoding/json"
    "io"
    d "backend/testnode/domain"
)

func DeserializeFromStream(stream io.ReadCloser) (d.Cat, error) {
    inst := &cat{}
    if err := json.NewDecoder(stream).Decode(inst); err == nil {
        return inst, nil
    } else {
        return nil, err
    }
}

type cat struct {
    NameF string    `json:"name" validate:"required"`
    AgeF int        `json:"age" validate:"required,gt=0"`
    IdF int         `json:"id"`
}

func (s *cat) Name() string {
    return s.NameF
}

func (s *cat) Age() int {
    return s.AgeF
}

func (s *cat) Id() int {
    return s.IdF
}

func (s *cat) MergeWith(anotherCat d.Cat) d.Cat {
    return anotherCat
}

func (s *cat) ToJson() ([]byte, error) {
    return json.Marshal(s)
}
