package cat

import (
    "database/sql"
    _ "github.com/lib/pq"
    c "backend/common"
    d "backend/testnode/domain"
)

func CreatePgStorage(conn c.PgConnection) (d.CatStorage, error) {
    inst := &pgStorage{
        conn: conn,
    }
    return inst, nil
}

type pgStorage struct {
    conn c.PgConnection
}

func foo(app d.App) {
}

func (s *pgStorage) Save(inst d.Cat) (d.Cat, error) {
    cat := inst.(*cat)
    err := s.conn.
        Client().
        QueryRow(`
            INSERT INTO cat
                (id, name, age)
            VALUES (COALESCE(NULLIF($1, 0), NEXTVAL('cat_ids')), $2, $3)
            ON CONFLICT (id) DO UPDATE
            SET name = EXCLUDED.name,
                age = EXCLUDED.age
            RETURNING id
        `, cat.IdF, cat.NameF, cat.AgeF).
        Scan(&cat.IdF)
    if err == nil {
        return cat, nil
    } else {
        return nil, err
    }
}

func (s *pgStorage) GetById(id int) (d.Cat, error) {
    cat := &cat{}
    err := s.conn.
        Client().
        QueryRow("SELECT id, name, age FROM cat WHERE id = $1", id).
        Scan(&cat.IdF, &cat.NameF, &cat.AgeF)
    if err == nil {
        return cat, nil
    } else if err == sql.ErrNoRows {
        return nil, CatNotFoundErr(id)
    } else {
        return nil, err
    }
}
