package cat

import (
    "log"
    "strconv"
    "net/http"
    "github.com/gorilla/mux"
    c "backend/common"
    d "backend/testnode/domain"
)

func NewCatRouter(app d.App) c.RouterFactory {
    return func(prefix string) (http.Handler, error) {
        s := mux.NewRouter().PathPrefix(prefix).Subrouter()
        s.Methods(c.HTTP_GET).Path("/{id:[0-9]+}").HandlerFunc(GetById(app))
        s.Methods(c.HTTP_PUT).Path("/{id:[0-9]+}").HandlerFunc(Update(app))
        s.Methods(c.HTTP_POST).Path("/").HandlerFunc(CreateNew(app))
        return s, nil
    }
}

func CreateNew(app d.App) c.HttpHandler {
    return func(res http.ResponseWriter, req *http.Request) {
        respond := c.Respond(onSuccess(created(res)), onFailure(res))
        step := DeserializeCat(req)
        step = c.Next(step, c.Validate())
        step = c.Next(step, SaveCat(app))
        step = c.Next(step, c.SerializeToJson())
        respond(step)
    }
}

func Update(app d.App) c.HttpHandler {
    return func(res http.ResponseWriter, req *http.Request) {
        respond := c.Respond(onSuccess(res), onFailure(res))
        step := ParseCatWithId(req)
        step = c.Next(step, c.Validate())
        step = c.Next(step, FindAndMergeCat(app))
        step = c.Next(step, SaveCat(app))
        step = c.Next(step, c.SerializeToJson())
        respond(step)
    }
}

func GetById(app d.App) c.HttpHandler {
    return func(res http.ResponseWriter, req *http.Request) {
        respond := c.Respond(onSuccess(res), onFailure(res))
        step := ParseCatId(req)
        step = c.Next(step, GetCatById(app))
        step = c.Next(step, c.SerializeToJson())
        respond(step)
    }
}

func created(res http.ResponseWriter) http.ResponseWriter {
    res.WriteHeader(http.StatusCreated)
    return res
}

func onSuccess(res http.ResponseWriter) c.ResponseHandler {
    return func(data interface{}) {
        stream := data.([]byte)
        res.Write(stream)
    }
}

func onFailure(res http.ResponseWriter) c.ResponseHandler {
    return func(err interface{}) {
        if e, ok := err.(*c.ValidationFailed); ok {
            res.WriteHeader(http.StatusBadRequest)
            log.Print(e)
        } else if e, ok := err.(*CatNotFound); ok {
            res.WriteHeader(http.StatusNotFound)
            log.Print(e)
        } else {
            res.WriteHeader(
                http.StatusInternalServerError)
            log.Printf(err.(error).Error())
        }
    }
}

func ParseCatId(req *http.Request) c.Monad {
    return func(data c.Data) (c.Data, error) {
        id, _ := strconv.Atoi(mux.Vars(req)["id"])
        if id > 0 {
            return id, nil
        } else {
            return nil, c.ValidationFailedErr(
                "Cat ID not provided or invalid",
            )
        }
    }
}

func DeserializeCat(req *http.Request) c.Monad {
    return func(data c.Data) (c.Data, error) {
        return DeserializeFromStream(req.Body)
    }
}

func MatchCatId(catId int, cat d.Cat) c.Monad {
    return func(data c.Data) (c.Data, error) {
        if cat.Id() == catId {
            return cat, nil
        } else {
            return nil, c.ValidationFailedErr(
                "Cat ID mismatch",
            )
        }
    }
}

func ParseCatWithId(req *http.Request) c.Monad {
    return func(data c.Data) (c.Data, error) {
        catId, catBody, err := c.ExecTwo(ParseCatId(req), DeserializeCat(req))
        match := MatchCatId(catId.(int), catBody.(d.Cat))
        return c.Exec(c.Next(c.Check(err), match))
    }
}
