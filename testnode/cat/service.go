package cat

import (
    c "backend/common"
    d "backend/testnode/domain"
)

func SaveCat(app d.App) c.Monad {
    return func(data c.Data) (c.Data, error) {
        cat := data.(d.Cat)
        return app.Storage().Cat().Save(cat)
    }
}

func GetCatById(app d.App) c.Monad {
    return func(data c.Data) (c.Data, error) {
        catId := data.(int)
        return app.Storage().Cat().GetById(catId)
    }
}

func MergeCatWith(that d.Cat) c.Monad {
    return func(data c.Data) (c.Data, error) {
        this := data.(d.Cat)
        return this.MergeWith(that), nil
    }
}

func FindAndMergeCat(app d.App) c.Monad {
    return func(data c.Data) (c.Data, error) {
        thatCat := data.(d.Cat)
        exec := GetCatById(app)
        exec = c.Next(exec, MergeCatWith(thatCat))
        return exec(thatCat.Id())
    }
}
