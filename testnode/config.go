package testnode

import (
    "fmt"
    c "backend/common"
    d "backend/testnode/domain"
)

type conf struct {
    Server struct {
        Host string         `json:"host" validate:"required"`
        Port int            `json:"port" validate:"required,gt=1000"`
    }                       `json:"server" validate:"required"`
    Postgres struct {
        Connection string   `json:"connection" validate:"required"`
    }                       `json:"postgres" validate:"required"`
}

func ReadConfig(filename string) (d.Config, error) {
    step := c.OpenFile(filename)
    step = c.Next(step, c.DecodeJson(&conf{}))
    step = c.Next(step, c.Validate())
    if data, err := c.Exec(step); err == nil {
        return data.(*conf), nil
    } else {
        return nil, err
    }
}

func (s *conf) Hostname() string {
    return fmt.Sprintf("%s:%d", s.Server.Host, s.Server.Port)
}

func (s *conf) PostgresConnection() string {
    return s.Postgres.Connection
}
