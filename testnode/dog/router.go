package dog

import (
    "net/http"
    "github.com/gorilla/mux"
    c "backend/common"
    d "backend/testnode/domain"
)

func NewDogRouter(app d.App) c.RouterFactory {
    return func(prefix string) (http.Handler, error) {
        s := mux.NewRouter().PathPrefix(prefix).Subrouter()
        s.Methods(c.HTTP_GET).Path("/wow").HandlerFunc(Wow())
        return s, nil
    }
}

func Wow() c.HttpHandler {
    return func(res http.ResponseWriter, req *http.Request) {
        res.Write([]byte("wow"))
    }
}
