package domain

type App interface {
    Config() Config
    Storage() Storage
}

type Config interface {
    Hostname() string
    PostgresConnection() string
}

type Storage interface {
    Cat() CatStorage
}
