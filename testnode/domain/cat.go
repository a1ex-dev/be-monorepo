package domain

type Cat interface {
    Id() int
    Name() string
    Age() int
    MergeWith(Cat) Cat
    ToJson() ([]byte, error)
}

type CatStorage interface {
    GetById(int) (Cat, error)
    Save(Cat) (Cat, error)
}
