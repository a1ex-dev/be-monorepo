package testnode

import (
    "backend/testnode/cat"
    c "backend/common"
    d "backend/testnode/domain"
)

func CreateStorage(conf d.Config) (d.Storage, error) {
    if pgConn, err := c.CreatePgConnection(conf.PostgresConnection()); err == nil {
        if catStorage, err := cat.CreatePgStorage(pgConn); err == nil {
            inst := &storage{
                cat: catStorage,
            }
            return inst, nil
        } else {
            return nil, err
        }
    } else {
        return nil, err
    }
}

type storage struct {
    cat d.CatStorage
}

func (s *storage) Cat() d.CatStorage {
    return s.cat
}
